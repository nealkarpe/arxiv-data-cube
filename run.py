#!/usr/bin/env python
from app import app

def main():
	port = 3000
	app.run(debug=True, host="127.0.0.1", port=port)

if __name__ == '__main__':
	main()