import json,re
from pprint import pprint
from collections import OrderedDict

with open("mod_data.json") as f:
    data = json.load(f)

# pprint(data)

tag_dict = {}
paper_dict = {}

for i in range(len(data)):
    sample = data[i]
    tag_list = eval(sample['tag'])
    for tag in tag_list:
        tagname = tag['term']
        if(re.findall("[A-Z]\.[0-9]*m*\.*[0-9]*m*",tagname)):
            # tag_arr1 = tagname.split(';')
            # tag_arr2 = []
            # tag_arr3 = []
            # if(len(tag_arr1[0]) == len(tagname)):
            #     tag_arr2 = tagname.split(',')
            #     # print("This is array",tag_arr2)
            # tag_arr = []
            # for st in tag_arr1:
            #     if ',' not in st:
            #         tag_arr.append(st)
            # for st in tag_arr2:
            #     if ';' not in st:
            #         tag_arr.append(st)
            # tag_arr = [x.strip(' \n') for x in tag_arr]
            tag_arr = re.split("[,; ]+",tagname)
            # print("This is array",tag_arr2)
            for t in tag_arr:
                if t not in tag_dict and re.findall("[A-Z]\.[0-9]*m*\.*[0-9]*m*",t):
                        # if t not in tag_dict:
                    tag_dict[t] = 1
                    paper_dict[t] = [sample['id']]
                elif re.findall("[A-Z]\.[0-9]*m*\.*[0-9]*m*",t):
                    tag_dict[t] += 1
                    if(sample['id'] not in paper_dict[t]):
                        paper_dict[t].append(sample['id'])

tag_dict = sorted(tag_dict.items(), key=lambda kv: -kv[1])
cnt_f_parent = {'A':2,'B':8,'C':5,'D':4,'E':5,'F':4,'G':4,'H':5,'I':7,'J':7,'K':8}
keys = ['A','B','C','D','E','F','G','H','I','J','K']
for i in range(11):
  for j in range(cnt_f_parent[keys[i]]+1):
    # for l in paper_dict:
    pat = str(keys[i])+"\."+str(j)+"\."
    if(str(keys[i])+"."+str(j) not in paper_dict):
        paper_dict[str(keys[i])+"."+str(j)] = []
    if(str(keys[i]+".m") not in paper_dict):
        paper_dict[str(keys[i])+".m"] = []
    if(str(keys[i]) not in paper_dict):
        paper_dict[str(keys[i])] = []
        # if re.findall(pat,l):
        #     if(str(keys[i])+"."+str(j) not in paper_dict):
        #         continue
        #     else:
        #         paper_dict[str(keys[i])+"."+str(j)].extend(paper_dict[l])
for i in range(11):
  for j in range(cnt_f_parent[keys[i]]+1):
    for l in paper_dict:
        pat = str(keys[i])+"\."+str(j)+"\."
        if re.findall(pat,l):
            paper_dict[str(keys[i])+"."+str(j)].extend(paper_dict[l])
for i in range(11):
  for l in paper_dict:
    pat = str(keys[i])+"\.[0-9]$"
    if re.findall(pat,l):
        paper_dict[str(keys[i])].extend(paper_dict[l])
    pat = str(keys[i])+"\.m$"
    if re.findall(pat,l):
        paper_dict[str(keys[i])].extend(paper_dict[l])

for i in paper_dict:
    paper_dict[i] = list(set(paper_dict[i]))
# pprint(paper_dict)

paper_dict["all"] = []
for l in paper_dict:
    paper_dict["all"].extend(paper_dict[l])

for i in paper_dict:
    paper_dict[i] = list(set(paper_dict[i]))
pprint(len(paper_dict["K.4.3"]))
# tag_dict = tag_dict[:4]
# pprint(paper_dict)
# for key,value in tag_dict:
#     if(re.findall("[A-Z]\.[0-9]\.m$",key)):
#         print(key,value)
# tag_list = []
# for tagname in tag_dict:
#   tag_list.append(tagname[0])

# def select(query_tag_set, data):
#   """ returns list of samples which have all tags in tag_set"""
#   count = 0
#   for i in range(len(data)):
#       sample = data[i]
#       tag_list = eval(sample['tag'])
#       sample_tag_set = set()
#       for tag in tag_list:
#           tagname = tag['term']
#           sample_tag_set.add(tagname)
#       if query_tag_set.intersection(sample_tag_set) == query_tag_set:
#           count += 1
#   return count

# def tag_subsets(tag_list):
#   """returns list of subsets of tag_set"""
#   if tag_list==[]:
#       s = set()
#       return [s]
#   next_tag_subsets_1 = tag_subsets(tag_list[1:])
#   next_tag_subsets_2 = []
#   for subset in next_tag_subsets_1:
#       next_tag_subsets_2.append(subset.union({tag_list[0]}))
#   return next_tag_subsets_1 + next_tag_subsets_2

# all_subsets = sorted(tag_subsets(tag_list)[1:],key = len)

# subset_count = {}
# for subset in all_subsets:
#   count = select(subset, data)
#   subset_count[str(subset)] = count
#   # pprintt(subset, count)


# # print('-----------------------Data cube counts----------------------------')
# subset_count = sorted(subset_count.items(), key=lambda kv: -kv[1])
# pprint(subset_count)
