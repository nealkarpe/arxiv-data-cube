# script to generate the storage json files for num_tags in range [2,8]

import json
from itertools import chain, combinations

with open("arxivData.json") as f:
    data = json.load(f)

def select(query_tag_set, data):
    """ returns list of samples which have all tags in tag_set"""
    out = []
    for sample in data:
        tag_list = eval(sample['tag'])
        sample_tag_set = set()
        for tag in tag_list:
            tagname = tag['term']
            sample_tag_set.add(tagname)
        if query_tag_set.intersection(sample_tag_set) == query_tag_set:
            date = "/".join([str(sample['day']),str(sample['month']),str(sample['year'])])
            out.append({'id': sample['id'], 'title': sample['title'], 'date': date})
    return out

def powerset(tag_list):
    """ returns list of all subsets of tag_list"""
    return list(chain.from_iterable(combinations(tag_list, r) for r in range(len(tag_list)+1)))

def set_to_str(tag_set):
    """ returns string representation of tag_set """
    ret = "["
    for tagname in tag_set:
        ret += (tagname+",")
    if ret[-1]==",":
        ret = ret[:-1] + "]"
    else:
        ret += "]"
    return ret

def create_graph_data(tag_list, all_subset_strings, subset_papers):
    lengths = []
    for i in range(len(tag_list)):
        lengths.append([])
    json_obj = {}
    json_obj["nodes"] = []
    arr = []
    for subset in all_subset_strings:
        n = len(subset[1:-1].split(','))
        if len(subset[1:-1].split(',')[0])==0:
            continue
        temp = {}
        temp["id"] = subset
        temp["support"] = len(subset_papers[subset])
        temp["group"] = n+1
        temp["url"] = subset
        lengths[n-1].append(subset[1:-1].split(','))
        temp["x"] = len(lengths[n-1])
        temp["y"] = n
        arr.append(temp)
    json_obj["nodes"] = [{"group": 1,"id": "NULL", "x":0, "y":0, "url": "[]", "support":len(subset_papers["[]"])}]
    json_obj["nodes"]+=arr
    json_obj["links"] = []
    first_term = 100
    difference = 20
    for i in range(len(lengths[0])):
        temp = {}
        temp["source"] = "NULL"
        temp["target"] = "[" + ",".join(lengths[0][i]) + "]"
        temp["value"] = first_term
        temp["fixed"] = True
        json_obj["links"].append(temp)
    for i in range(len(tag_list)-1):
        for j in range(len(lengths[i])):
            for k in range(len(lengths[i+1])):
                if len(set(lengths[i][j]).intersection(set(lengths[i+1][k])))==len(set(lengths[i][j])):
                    temp = {}
                    temp["source"] = "[" + ",".join(lengths[i][j]) + "]"
                    temp["target"] = "[" + ",".join(lengths[i+1][k]) + "]"
                    temp["value"] = first_term + len(lengths[i][j])*difference
                    temp["fixed"] = True
                    json_obj["links"].append(temp)
    json_data = json.dumps(json_obj)
    return json_data

tag_dict = {}
id_to_ind = {}
for i in range(len(data)):
    sample = data[i]
    id_to_ind[sample['id']] = i
    tag_list = eval(sample['tag'])
    for tag in tag_list:
        tagname = tag['term']
        if tagname not in tag_dict:
            tag_dict[tagname] = 1
        else:
            tag_dict[tagname] += 1
tag_dict = sorted(tag_dict.items(), key=lambda kv: -kv[1])

def generate(num_tags):
    curr_tag_dict = tag_dict[:num_tags]
    tag_list = [tagname[0] for tagname in curr_tag_dict]
    all_subsets = powerset(tag_list)
    all_subset_strings = []
    subset_papers = {}
    for subset in all_subsets:
        papers = select(set(subset), data)
        subset_string = set_to_str(subset)
        subset_papers[subset_string] = papers
        all_subset_strings.append(subset_string)
    graph_data = create_graph_data(tag_list, all_subset_strings, subset_papers)
    return subset_papers, graph_data 

for i in range(2,9):
    print(i)
    subset_papers, graph_data = generate(i)
    out = {}
    out['subset_papers'] = subset_papers
    out['graph_data'] = graph_data
    filename = "app/storage/"+str(i)+".json"
    with open(filename, 'w') as f:
        json.dump(out,f,indent=4)