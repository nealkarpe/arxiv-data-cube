from flask import Flask, request, redirect, url_for, render_template
import json, re
from itertools import chain, combinations

with open("arxivData.json") as f:
    data = json.load(f)

with open("acm_data.json") as f:
    acm_data = json.load(f)

def select(query_tag_set, data):
    """ returns list of samples which have all tags in tag_set"""
    out = []
    for sample in data:
        tag_list = eval(sample['tag'])
        sample_tag_set = set()
        for tag in tag_list:
            tagname = tag['term']
            sample_tag_set.add(tagname)
        if query_tag_set.intersection(sample_tag_set) == query_tag_set:
            date = "/".join([str(sample['day']),str(sample['month']),str(sample['year'])])
            out.append({'id': sample['id'], 'title': sample['title'], 'date': date})
    return out

def powerset(tag_list):
    """ returns list of all subsets of tag_list"""
    return list(chain.from_iterable(combinations(tag_list, r) for r in range(len(tag_list)+1)))

def set_to_str(tag_set):
    """ returns string representation of tag_set """
    ret = "["
    for tagname in tag_set:
        ret += (tagname+",")
    if ret[-1]==",":
        ret = ret[:-1] + "]"
    else:
        ret += "]"
    return ret

def create_graph_data(tag_list, all_subset_strings):
    lengths = []
    for i in range(len(tag_list)):
        lengths.append([])
    json_obj = {}
    json_obj["nodes"] = []
    arr = []
    for subset in all_subset_strings:
        n = len(subset[1:-1].split(','))
        if len(subset[1:-1].split(',')[0])==0:
            continue
        temp = {}
        temp["id"] = subset
        temp["group"] = n+1
        temp["url"] = subset
        lengths[n-1].append(subset[1:-1].split(','))
        temp["x"] = len(lengths[n-1])
        temp["y"] = n
        arr.append(temp)
    json_obj["nodes"] = [{"group": 1,"id": "NULL", "x":0, "y":0, "url": "[]"}]
    json_obj["nodes"]+=arr
    json_obj["links"] = []
    first_term = 100
    difference = 20
    for i in range(len(lengths[0])):
        temp = {}
        temp["source"] = "NULL"
        temp["target"] = "[" + ",".join(lengths[0][i]) + "]"
        temp["value"] = first_term
        temp["fixed"] = True
        json_obj["links"].append(temp)
    for i in range(len(tag_list)-1):
        for j in range(len(lengths[i])):
            for k in range(len(lengths[i+1])):
                if len(set(lengths[i][j]).intersection(set(lengths[i+1][k])))==len(set(lengths[i][j])):
                    temp = {}
                    temp["source"] = "[" + ",".join(lengths[i][j]) + "]"
                    temp["target"] = "[" + ",".join(lengths[i+1][k]) + "]"
                    temp["value"] = first_term + len(lengths[i][j])*difference
                    temp["fixed"] = True
                    json_obj["links"].append(temp)
    json_data = json.dumps(json_obj)
    return json_data

tag_dict = {}
id_to_ind = {}
for i in range(len(data)):
    sample = data[i]
    id_to_ind[sample['id']] = i
    tag_list = eval(sample['tag'])
    for tag in tag_list:
        tagname = tag['term']
        if tagname not in tag_dict:
            tag_dict[tagname] = 1
        else:
            tag_dict[tagname] += 1
tag_dict = sorted(tag_dict.items(), key=lambda kv: -kv[1])

def generate(num_tags):
    try:
        filename = "app/storage/"+str(num_tags)+".json"
        with open(filename) as f:
            result = json.load(f)
        subset_papers = result['subset_papers']
        graph_data = result['graph_data']
    except:
        curr_tag_dict = tag_dict[:num_tags]
        tag_list = [tagname[0] for tagname in curr_tag_dict]
        all_subsets = powerset(tag_list)
        all_subset_strings = []
        subset_papers = {}
        for subset in all_subsets:
            papers = select(set(subset), data)
            subset_string = set_to_str(subset)
            subset_papers[subset_string] = papers
            all_subset_strings.append(subset_string)
        graph_data = create_graph_data(tag_list, all_subset_strings)
    return subset_papers, graph_data
curr_tags = 2
subset_papers, graph_data = generate(curr_tags)

def acm_generate():
    acm_tag_dict = {}
    acm_paper_dict = {}
    acm_id_to_ind = {}
    for i in range(len(acm_data)):
        sample = acm_data[i]
        acm_id_to_ind[sample['id']] = i
        tag_list = eval(sample['tag'])
        for tag in tag_list:
            tagname = tag['term']
            if(re.findall("[A-Z]\.[0-9]*m*\.*[0-9]*m*",tagname)):
                tag_arr = re.split("[,; ]+",tagname)
                for t in tag_arr:
                    if t not in acm_tag_dict and re.findall("[A-Z]\.[0-9]*m*\.*[0-9]*m*",t):
                        acm_tag_dict[t] = 1
                        acm_paper_dict[t] = [sample['id']]
                    elif re.findall("[A-Z]\.[0-9]*m*\.*[0-9]*m*",t):
                        acm_tag_dict[t] += 1
                        if(sample['id'] not in acm_paper_dict[t]):
                            acm_paper_dict[t].append(sample['id'])
    acm_tag_dict = sorted(acm_tag_dict.items(), key=lambda kv: -kv[1])
    cnt_f_parent = {'A':2,'B':8,'C':5,'D':4,'E':5,'F':4,'G':4,'H':5,'I':7,'J':7,'K':8}
    keys = ['A','B','C','D','E','F','G','H','I','J','K']
    for i in range(11):
      for j in range(cnt_f_parent[keys[i]]+1):
        pat = str(keys[i])+"\."+str(j)+"\."
        if(str(keys[i])+"."+str(j) not in acm_paper_dict):
            acm_paper_dict[str(keys[i])+"."+str(j)] = []
        if(str(keys[i])+"."+str(j)+".m" not in acm_paper_dict):
            acm_paper_dict[str(keys[i])+"."+str(j)+".m"] = []    
        if(str(keys[i]+".m") not in acm_paper_dict):
            acm_paper_dict[str(keys[i])+".m"] = []
        if(str(keys[i]) not in acm_paper_dict):
            acm_paper_dict[str(keys[i])] = []
    for i in range(11):
      for j in range(cnt_f_parent[keys[i]]+1):
        for l in acm_paper_dict:
            pat = str(keys[i])+"\."+str(j)+"\."
            if re.findall(pat,l):
                acm_paper_dict[str(keys[i])+"."+str(j)].extend(acm_paper_dict[l])
    for i in range(11):
      for l in acm_paper_dict:
        pat = str(keys[i])+"\.[0-9]$"
        if re.findall(pat,l):
            acm_paper_dict[str(keys[i])].extend(acm_paper_dict[l])
        pat = str(keys[i])+"\.m$"
        if re.findall(pat,l):
            acm_paper_dict[str(keys[i])].extend(acm_paper_dict[l])
    for i in acm_paper_dict:
        acm_paper_dict[i] = list(set(acm_paper_dict[i]))
    acm_paper_dict["all"] = []
    for l in acm_paper_dict:
        acm_paper_dict["all"].extend(acm_paper_dict[l])
    for i in acm_paper_dict:
        acm_paper_dict[i] = list(set(acm_paper_dict[i]))
    return acm_paper_dict, acm_id_to_ind

acm_paper_dict, acm_id_to_ind = acm_generate()

# this creates our Flask app
app = Flask(__name__, static_url_path='/static')

@app.route("/cube")
def get_graph_data():
    return graph_data

@app.route('/request_data_cube', methods=['POST'])
def request_data_cube_with_num_tags():
    num_tags = int(request.form.get('num_tags'))
    global curr_tags, subset_papers, graph_data
    curr_tags = num_tags
    subset_papers, graph_data = generate(curr_tags)
    return redirect(url_for('home'))

@app.route('/', methods=['GET'])
def home():
    data_cube_dimensions = {2:(300, 0), 3:(450, 0), 4:(560, 0), 5:(730,+25), 6:(965, 65), 7:(1250, 110), 8:(1580, 150)}
    return render_template('index.html', title="arXiv Data Cube", num_tags=curr_tags, top_tags=tag_dict[:8], data_cube_dimensions=data_cube_dimensions)

@app.errorhandler(404)
def error(error):
    return render_template('error.html', title="Oops! Nothing here")
    
@app.errorhandler(KeyError)
def error(error):
    return render_template('error.html', title="Oops! No papers here")

@app.route('/tag_set_papers/<tag_set>', methods=['GET'])
def get_tag_set_papers(tag_set):
    title = "Papers having "+ tag_set + " tags"
    return render_template('tag_set_papers.html', title=title, papers=subset_papers[tag_set], tag_set=tag_set)

@app.route('/paper/<paper_id>', methods=['GET'])
def get_paper(paper_id):
    title = "Paper information: " + paper_id
    sample = data[id_to_ind[paper_id]]
    authors = ", ".join([x['name'] for x in eval(sample['author'])])
    tags = ", ".join([x['term'] for x in eval(sample['tag'])])
    link = eval(sample['link'])[0]['href']
    date = "/".join([str(sample['day']),str(sample['month']),str(sample['year'])])
    other_fields = ['id', 'title', 'summary']
    paper_dict = {field:sample[field] for field in other_fields}
    return render_template('paper.html', title=title, paper_dict=paper_dict, date=date, authors=authors, link=link, tags=tags)

@app.route('/acm', methods=['GET'])
def acm_home():
    return render_template('acm.html', title="ACM Codes Tree")

@app.route('/acm/code_papers/<code>', methods=['GET'])
def get_acm_code_papers(code):
    title = "Papers having ACM code: " + code
    papers = []
    for paper_id in acm_paper_dict[code]:
        tmp = acm_data[acm_id_to_ind[paper_id]]
        tmp['date'] = str(tmp['day'])+'/'+str(tmp['month'])+'/'+str(tmp['year'])
        papers.append(tmp)
    return render_template('tag_set_papers.html', title=title, papers=papers, tag_set=code)
