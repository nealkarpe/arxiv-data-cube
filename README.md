# arXiv Data Cube

* Requirements
	* The only external package used is Flask. Install using `pip3 install Flask`.

* Running instructions
	* Run `python3 run.py` to start the server.
	* Open `localhost:3000` in browser to use the application.
